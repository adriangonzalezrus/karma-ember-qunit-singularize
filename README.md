# Qunit-Special-Blend

Ever want to run a single qunit test from your test suite with karma?
Yeah, so did I.

This plugin/wrapper/npm module/hack was created to allow you to run a single
qunit test or module with the karma test runner.

## Installation

`npm install karma-qunit-special-blend`

## Usage

After the package is installed configure your frameworks array in `karma.conf.js` as
follows:

``` javascript
  frameworks: ['qunit', 'qunit-sb'],
```

You will now be able to use three new functions in your tests.js files
- `singleTest`
- `singleModule`
- `singleAsyncTest`
in addition to the regular qunit `test` and `module` methods.

If you don't have any of the new methods in your test files all tests will run
as usual. If you have used the new methods only those tests will run.


## Vim plugin to make this more convenient

I don't want to change `test` to `singleTest` and back all the time in my test
files file, because this is supposed to make things faster and easier after
all. I also happen to use Vim so I have written the [vim-qunit-special-blend](https://github.com/JarrodCTaylor/vim-qunit-special-blend)
plugin that leverages the functionality provided by this npm module to run
single tests but allows you to do it without altering your test code.
