var testStack = [];
var runSubset = false;
var insideRunSubsetModule = false;

function module() {
  insideRunSubsetModule = false;
  pushFuncToStack(QUnit.module, arguments, true);
}

function asyncTest() {
  pushFuncToStack(QUnit.asyncTest, arguments, insideRunSubsetModule);
}

function test() {
  pushFuncToStack(QUnit.test, arguments, insideRunSubsetModule);
}

function singleModule() {
  runSubset = true;
  insideRunSubsetModule = true;
  pushFuncToStack(QUnit.module, arguments, true);
}

function singleAsyncTest() {
  runSubset = true;
  pushFuncToStack(QUnit.asyncTest, arguments, true);
}

function singleTest() {
  runSubset = true;
  pushFuncToStack(QUnit.test, arguments, true);
}

function pushFuncToStack(func, args, run) {
  testStack.push({func: func, args: args, run: run});
}

function specialBlend() {
  var stackLen = testStack.length;
  for (var i=0; i<stackLen; i++) {
    var testToRun = testStack[i];
    if (!runSubset || testToRun.run) {
      testToRun.func.apply(window, testToRun.args);
    }
  }
}
