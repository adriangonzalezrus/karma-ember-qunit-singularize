(function() {

  var createPattern = function(path) {
    return {pattern: path, included: true, served: true, watched: false};
  };

  var blendQunit = function(files) {
    qunit = files.shift();
    files.unshift(createPattern(__dirname + '/qunit-special-blend.js'));
    files.unshift(qunit);
    files.push(createPattern(__dirname + '/run-qunit-special-blend.js'));
  };

  blendQunit.$inject = ['config.files'];

  module.exports = {
      'framework:qunit-sb': ['factory', blendQunit],
  };

}).call(this);
